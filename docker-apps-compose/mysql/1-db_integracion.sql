/*
SQLyog Community
MySQL - 5.7.28 : Database - db_integracion
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_integracion` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_integracion`;

/*Table structure for table `business_managment_config` */

CREATE TABLE `business_managment_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `netsuite_endpoint_id` varchar(100) NOT NULL,
  `script_before` longtext,
  `script_after` longtext,
  `netsuite_url` varchar(250) DEFAULT NULL,
  `security` int(11) DEFAULT NULL COMMENT '0 = publico , 1 = privado',
  `auth_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `business_managment_config` */

insert  into `business_managment_config`(`id`,`netsuite_endpoint_id`,`script_before`,`script_after`,`netsuite_url`,`security`,`auth_type`) values 
(1,'/vacaciones/empleado','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=277&deploy=1',0,'OAUTH1'),
(2,'/vacaciones/historico','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=282&deploy=1',0,'OAUTH1'),
(3,'/supervisores','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=278&deploy=1',0,'OAUTH1'),
(4,'/lead','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=296&deploy=1',0,'OAUTH1'),
(5,'/documento/validacionDeuda','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=305&deploy=1',0,'OAUTH1'),
(6,'/documento/cancelacionDeuda','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=301&deploy=1',0,'OAUTH1'),
(7,'/alumno/actualizacion','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=363&deploy=1',0,'OAUTH1'),
(8,'/admision/postulante','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=384&deploy=1',0,'OAUTH1'),
(9,'/escalas/periodo','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=389&deploy=1',0,'OAUTH1'),
(10,'/documento/crearInvoice','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=376&deploy=1',0,'OAUTH1'),
(11,'/consulta/catalogo/documAcad','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=375&deploy=1',0,'OAUTH1'),
(12,'/alumno/deuda/vencida','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=255&deploy=1',0,'OAUTH1'),
(13,'/alumno/estado/cuenta','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=254&deploy=1',0,'OAUTH1'),
(14,'/centrocosto','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=399&deploy=1',0,'OAUTH1'),
(15,'/empleado','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=401&deploy=1',0,'OAUTH1'),
(16,'/empleado/query','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=400&deploy=1',0,'OAUTH1'),
(17,'/lead/beutec','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=457&deploy=1',0,'OAUTH1'),
(19,'/lead/intereses','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=462&deploy=1',0,'OAUTH1'),
(20,'/libro-reclamacion','','','https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=473&deploy=1',0,'OAUTH1'),
(21,'/netsuite/alumno/update',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=514&deploy=1',0,'OAUTH1'),
(22,'/netsuite/alumno/create',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=515&deploy=1',0,'OAUTH1'),
(23,'/netsuite/alumno/foto/update',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=516&deploy=1',0,'OAUTH1'),
(24,'/netsuite/alumno/search',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=517&deploy=1',0,'OAUTH1'),
(25,'/netsuite/oportunidad/create',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=519&deploy=1',0,'OAUTH1'),
(26,'/netsuite/oportunidad/update',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=524&deploy=1',0,'OAUTH1'),
(27,'/netsuite/moveon/programas',NULL,NULL,'https://4535271-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=525&deploy=1',0,'OAUTH1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
