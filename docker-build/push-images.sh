image_name=$1
version=$2
private_registry=$3

docker tag $image_name:$version $private_registry/$image_name:$version
# docker tag $image_name:latest $private_registry/$image_name:latest
docker push $private_registry/$image_name:$version
# docker push $private_registry/$image_name:latest
