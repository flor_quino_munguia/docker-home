# UTEC Docker Images

# Requirements

- git
- docker
- docker compose

# Quickly build , tag and push

```
export image_name=debian-java8-tomcat
export private_registry=hubdocker.utec.edu.pe
export version=latest

docker build --rm -t $image_name:$version .
docker tag $image_name:$version $private_registry/$image_name:$version
docker push $private_registry/$image_name:$version
```

# Just run

- Login to hubdocker.utec.edu.pe
- Choose some of the prepared docker-compose.yml and execute
```

docker-compose up -d -f docker-compose-calendar.yml
```


# Docker Development

If you need to modify some image, you just need to clone the respective image, modify it and execute:

- docker local build
- docker local tag

Our continuous integration server will do the following:

- docker tag with private registry domain
- docker push image to private registry


# Apps Development

Similar to **Just run** option. But in this case you must comment or delete the app in composer yml file.
For example. If you need to modify student-api java api, you must:

- choose the **docker-compose-enrollment.yml**
- open and delete or comment the **student-api** section and execute:

```
docker-compose up -d -f docker-compose-calendar.yml
```
- open your favorite IDE and startup **student-api**

# Global steps

- Clone all repositories

```
git clone --recurse-submodules  git@bitbucket.org:utecsup/docker-home.git
```

# Steps for java builds

- Download **jdk-7u80-linux-x64.tar.gz** from [](https://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html) and save in **ubuntu-java7** folder

- Download **UnlimitedJCEPolicyJDK7.zip** from [UnlimitedJCEPolicyJDK7.zip](https://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html) and save in **ubuntu-java7** folder

# Build for java 7 apps

```
bash build-tag-images.sh "/../docker-images-base"  "/../docker-image-generic-java-7-war-app/build-images-order.txt" "1.0.0"
```


# [Run Java Apps](./docker-images-generic/docker-image-generic-java-7-war/readme.md)
