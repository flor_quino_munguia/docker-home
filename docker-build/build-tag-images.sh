#!/bin/bash

# Not recommended, will break on whitespace

images_folder=$1

if [ "x$images_folder" == "x" ] ;
then
  echo "images_folder is required."
  exit 1
fi

images_order_file=$2

if [ "x$images_order_file" == "x" ] ;
then
  echo "images_order_file is required."
  exit 1
fi

version=$3

if [ "x$version" == "x" ] ;
then
  echo "version is required."
  exit 1
fi


TMPDIR=`dirname $(mktemp -u -t tmp.XXXXXXXXXX)`
WORK_DIR=$images_folder
success_build_report=$TMPDIR/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
failure_build_report=$TMPDIR/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

while read directory_args; do
  echo ""
  echo ""

  args=
  startIndex=$(echo $directory_args | grep -bo "," | head -1 | sed 's/:.*$//')

  if [ "x$startIndex" = "x" ] ;
  then
      directory=$directory_args
  else
      lenght=${#directory_args}
      directory=${directory_args:0:startIndex}
      startIndex=$(( startIndex + 1))
      args=${directory_args:startIndex:lenght}
  fi

  directory=`echo $directory | sed 's/\\r//g'`

  echo "#################"
  echo "Docker image name: $directory"
  echo "Docker build args: $args"
  echo "Docker expected build folder :$WORK_DIR/$directory"
  echo ""

  cd $WORK_DIR/
  cd $directory

  status=

  (
    image_name=$(pwd | xargs basename)
    echo "Building started"
    if [ "x$args" = "x" ] ;
    then
      dockerCmd="docker build --rm -t $image_name:$version ."
      echo $dockerCmd
      eval $dockerCmd
    else
      dockerCmd="docker build --rm -t $image_name:$version $args ."
      echo $dockerCmd
      eval $dockerCmd
    fi

    echo ""
    echo "Tag started"
    echo ""
    dockerCmd="docker tag ${image_name}:${version} ${image_name}:${version}"
    dockerCmd="docker tag ${image_name}:${version} ${image_name}:latest"
    echo $dockerCmd
    eval $dockerCmd
  )
  status=$?

  if [ $status -eq 0 ]; then
    printf "%-70s | %-70s \n" "$directory" "success" >> $success_build_report
  else
    printf "%-70s | %-70s \n" "$directory" "failure" >> $failure_build_report
  fi
done <$images_order_file

echo ""
echo ""
echo "Docker images building status:"
echo ""
echo ""

if [ -e $success_build_report ]
then
  cat $success_build_report
fi

if [ -e $failure_build_report ]
then
  cat $failure_build_report
fi

echo ""
echo ""
