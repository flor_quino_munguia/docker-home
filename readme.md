# UTEC Docker Home

This repository contains our docker images used for our solutions. Our aim is to share our attempts to create a good piece of software.


# Requirements

- Linux
- docker
- docker compose(https://docs.docker.com/compose/install/)
- docker login hubdocker.utec.edu.pe

# Create global access

```
git clone ...
sudo ln -s $(pwd)/docky.sh /usr/bin/docky
sudo chmod +x /usr/bin/docky
```

# Configure docky

- Execute:

```
docky
```

- Select option 0, wait and read carefully the log

# start applications

- Execute:

```
docky
```

- Select option 1
- Enter our demo app

```
docker-utec-hello-world
```

Ahora si la descarga empieza y la app inicia correctamente, indicara que :

- docky esta correctamente configurado
- docker login credentials are ok
- validamos el log de la app

```
docker logs docker-utec-hello-world -f
```

Se debería ver un mensaje "Welcome to UTEC ..."


# Avanzado

La aplicacion anterior solo es un ejemplo sencillo. Para poder levantar las aplicaciones utec, necesitas configurar muchas cosas inicialmente:

- Variables (*****) en `$HOME/.docker-helper/environment`. Asignarle cualquier valor, excepto para la variable **ZOOKEEPER_SERVER_PASSWORD**. Preguntar al team por el valores adecuado
- zookeeper. Sigue esta [guia](./docker-apps/zookeeper/)

# Local build and deploy

Con esta funcionalidad podras buildear y deployar localmente las aplicaciones que esten correctamente dockerizadas.

> Download base images

```
docker pull hubdocker.utec.edu.pe/debian-java7-maven:latest
docker tag hubdocker.utec.edu.pe/debian-java7-maven:latest debian-java7-maven:latest
```
```
docker pull hubdocker.utec.edu.pe/debian-java7-tomcat:latest
docker tag hubdocker.utec.edu.pe/debian-java7-tomcat:latest debian-java7-tomcat:latest
```

Ejemplo : Buildear y deployar localmente un container llamado core-educativo-dev de la imagen core-educativo (debe existir core-educativo-dev en el zookeeper)

```
docky 2 \
docker_img_generic:"docker-image-generic-java-7-war" \
source_code_path:"/../bibutcket-git-repositories/core-educativo/" \
image_name:core-educativo \
container_name:core-educativo-dev \
container_port:6666 \
build_command:"mvn clean package -DskipTests=true -U -f UTEC-Reactor/pom.xml" \
war_name:"UTEC-Web.war" \
additional_arguments:"artifact_extension=war"
```
