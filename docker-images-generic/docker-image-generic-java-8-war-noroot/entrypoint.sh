#!/bin/bash

mkdir -p /log
mkdir -p /env
source_code_path="/app"
log_file="/log/app.log"
war_name=${WAR_NAME}


function echo_log {
  DATE='date +%Y/%m/%d:%H:%M:%S'
  echo `$DATE`" $1"
  echo `$DATE`" $1" >> $log_file
}

function export_java_env {
  java_environments=""
  echo_log "## Exporting Java Environment Variables ##"

  while IFS='=' read -r name value ; do
    if [[ $name == *'JAVA_'* ]] && [[ $name != "JAVA_HOME" ]]; then
      java_environments="$java_environments  ${!name} ";
    fi
  done < <(env)
  export CATALINA_OPTS="$java_environments"

}

function download_env_variables {

  echo_log ""
  echo_log "##### Starting to download environment variables #####"

  http_response=$(curl -s -o env_variables -w "%{http_code}" --user ${ZKHELPER_USER}:${ZKHELPER_PASSWORD} ${ZKHELPER_URL})

  echo_log "## status:  $http_response ##"
  if [ $http_response == "200" ]; then
    cp env_variables /env/
    echo_log "## Exporting Environment Variables ##"
    source /env/env_variables
    export_java_env
  else

    echo_log "## New Environment Variables could not be obtained ##"

    if [ -e /env/env_variables ]; then
      echo_log "## Exporting old env variables ##"
      source /env/env_variables
      export_java_env
    else
      echo_log "## Environment file not found in : /env/env_variables ##"
      exit 1
    fi
  fi

}

function start_app {
  echo_log ""
  echo_log "##### Starting app ##### "
  echo_log "## source_code_path: $source_code_path ##"

  if [ -n "$war_name" ]; then
    war_file_name=$war_name
    echo_log "## war argument detected : $war_name ##"
  else

    if [ "$(ls -A $source_code_path)" ]; then
      cd  $source_code_path/target
      war_file_number=$(find -name "*.war" | wc -l)
      war_file_name=
      if [ "$war_file_number" -gt "1" ] ; then
        echo_log "## Several war files were detected. We need just one!! ##"
        war_files=$(find -name "*.war")
        echo_log "$war_files"
        echo_log "## You should enter a unique war name. Example -e \"WAR_NAME=lassie.war\" ##"
        exit 1
      else
        # get unique war name
        war_file_name=$(find -name "*.war")
      fi
    fi
  fi
  cd /app/target
  echo_log "## war to deploy: $war_file_name ##"
  war_name="${war_file_name%.*}"

  if [ -d "/usr/local/tomcat/webapps/$war_name" ]
  then
    echo_log "## App is already deployed. ##"
  else
    echo_log "## Moving war to webapps: ##"
    ls -la
    echo_log "## cp $war_file_name /usr/local/tomcat/webapps/ ##"
    cp $war_file_name /usr/local/tomcat/webapps/
    echo_log "webapps content:"
    ls -la
  fi

  echo_log ""
  echo_log "##### Starting tomcat #####"
  echo_log ""
  bash /usr/local/tomcat/bin/catalina.sh run |& tee -a $log_file

}

########################
# Scripts starts here
########################
download_env_variables
start_app
