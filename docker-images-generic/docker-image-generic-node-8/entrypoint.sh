#!/bin/bash

mkdir -p /log
mkdir -p /env
source_code_path="/app"
log_file="/log/app.log"

function echo_log {
  DATE='date +%Y/%m/%d:%H:%M:%S'
  echo `$DATE`" $1"
  echo `$DATE`" $1" >> $log_file
}

function download_env_variables {

 echo_log ""
 echo_log "##### Starting to download environment variables #####"
 http_response=$(curl -s -o env_variables -w "%{http_code}" --user ${ZKHELPER_USER}:${ZKHELPER_PASSWORD} ${ZKHELPER_URL})

 echo_log "## status:  $http_response ##"
 if [ $http_response == "200" ]; then
    cp env_variables /env/
    echo_log "## Exporting Environment Variables ##"
    source /env/env_variables
 else

    echo_log "## New Environment Variables could not be obtained ##"

    if [ -e /env/env_variables ]; then
       echo_log "## Exporting old env variables ##"
       source /env/env_variables
    else
       echo_log "## Environment file not found in : /env/env_variables ##"
       exit 1
    fi
 fi

}

function start_app {
  echo_log ""
  echo_log "##### Starting app ##### "
  echo_log "## source_code_path: $source_code_path ##"

  npm run start

}

 ########################
 # Scripts starts here
 ########################

download_env_variables

start_app
