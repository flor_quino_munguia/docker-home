# docker-image-generic

This image could be used for applications in node 8 

# Build Steps

- copy these files to your app java source code:
    - .dockerignore
    - Dockerfile
    - entrypoint.sh

- open a shell pointing to the source code folder and build image with the following sentence:

```
export image_name=egresados-web
export image_version=1.0.0

#Now build the image
docker build --rm -t $image_name:$image_version .

```

# Tag image

- tag the image with with the following sentences:

```
export image_name=egresados-web
export image_version=1.0.0

docker tag  ${image_name}:${image_version} ${image_name}:${image_version}
docker tag  ${image_name}:${image_version} ${image_name}:latest
```

# Push image to private registry

```
# Only authorized users
bash /../../push-images.sh ${image_name} ${image_version} hubdocker.utec.edu.pe
```

# Run

```
# Tu ip (get your local ip)
export HOST_IP=10.1.36.189
export image_name=egresados-web
export APP_PORT=9000
export ZOOKEEPER_HELPER_API_PORT=2003
export ZOOKEEPER_HELPER_URL=http://${HOST_IP}:${ZOOKEEPER_HELPER_API_PORT}
export ZOOKEEPER_HELPER_USER=<YOUR USER>
export ZOOKEEPER_HELPER_PASS=<YOUR PASSWORD>

docker run -d \
--name ${image_name} \
-p ${APP_PORT}:8080 \
-e "ZKHELPER_URL=${ZOOKEEPER_HELPER_URL}/zookeeper/helper/znode/children/first/as-environment?path=/apps/${image_name}&globalpath=/global"  \
-e "ZKHELPER_USER=${ZOOKEEPER_HELPER_USER}" \
-e "ZKHELPER_PASSWORD=${ZOOKEEPER_HELPER_PASS}" \
-e TZ=America/Lima \
-i -t hubdocker.utec.edu.pe/${image_name}
```
