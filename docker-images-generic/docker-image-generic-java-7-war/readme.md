# docker-image-generic-java-7-war

This image could be used for any java 7 web application whose artifact is a war file.

# Build Steps

- copy these files to your app java source code:
    - .dockerignore
    - Dockerfile
    - entrypoint.sh

- open a shell pointing to the source code folder and build image with the following sentence:

```
export image_name=asistencia-docentes-api
export nexus_user=duke
export nexus_password=java

docker build --rm -t ${image_name}:1.0.0 \
--build-arg var_nexus_username=${nexus_user}  \
--build-arg var_nexus_password=${nexus_password}  \
--build-arg var_nexus_url=https://nexus-repository.utec.edu.pe/repository/maven-public/  \
--build-arg build_command="mvn clean package -Dskiptests=true" \
--build-arg artifact_extension=war .
```

# Tag image

- tag the image with with the following sentences:

```
export image_name=asistencia-docentes-api

docker tag  ${image_name}:1.0.0 ${image_name}:1.0.0
docker tag  ${image_name}:1.0.0 ${image_name}:latest
```

# Push image to private registry

```
export image_name=asistencia-docentes-api
bash /../../push-images.sh ${image_name} 1.0.0 hubdocker.utec.edu.pe
```

# Run

```
export image_name=asistencia-docentes-api

docker run -d \
--name ${image_name} \
-p ${ASISTENCIA_API_PORT}:8080 \
-e "ZKHELPER_URL=${ZOOKEEPER_HELPER_URL}/zookeeper/helper/znode/children/first/as-environment?path=/apps/${image_name}&globalpath=/global"  \
-e "ZKHELPER_USER=${ZOOKEEPER_HELPER_USER}" \
-e "ZKHELPER_PASSWORD=${ZOOKEEPER_HELPER_PASS}" \
-e "WAR_NAME=${image_name}.war" \
-e TZ=America/Lima \
-i -t hubdocker.utec.edu.pe/${image_name}
```
