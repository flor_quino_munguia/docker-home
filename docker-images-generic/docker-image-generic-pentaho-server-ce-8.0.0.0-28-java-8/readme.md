# docker-image-generic-java-7-war

This image could be used for any java 7 web application whose artifact is a war file.

# Before

- generar y copiar el archivo simple-file-manager.war y ojdbc7-12.1.0.1.jar

# Build Steps

- copy these files to your app java source code:
    - .dockerignore
    - Dockerfile
    - entrypoint.sh

- open a shell pointing to the source code folder and build image with the following sentence:

```
export image_name=docker-image-generic-pentaho-server-ce-8.0.0.0-28-java-8
docker build --rm -t ${image_name}:1.0.1 .
```

# Tag image

- tag the image with with the following sentences:

```
export image_name=docker-image-generic-pentaho-server-ce-8.0.0.0-28-java-8 1.0.0

docker tag  ${image_name}:1.0.0 ${image_name}:1.0.0
docker tag  ${image_name}:1.0.0 ${image_name}:latest
```

# Push image to private registry

```
export image_name=docker-image-generic-pentaho-server-ce-8.0.0.0-28-java-8 1.0.0
bash /../../push-images.sh ${image_name} 1.0.0 hubdocker.utec.edu.pe
```

# Run

```
export image_name=docker-image-generic-pentaho-server-ce-8.0.0.0-28-java-8 1.0.0
export PENTAHO_PORT=8085

docker run -d \
--name ${image_name} \
-p ${PENTAHO_PORT}:8080 \
-e "ZKHELPER_URL=${ZOOKEEPER_HELPER_URL}/zookeeper/helper/znode/children/first/as-environment?path=/apps/${image_name}&globalpath=/global"  \
-e "ZKHELPER_USER=${ZOOKEEPER_HELPER_USER}" \
-e "ZKHELPER_PASSWORD=${ZOOKEEPER_HELPER_PASS}" \
-e TZ=America/Lima \
-i -t hubdocker.utec.edu.pe/${image_name}
```


```
export image_name=docker-image-generic-pentaho-server-ce-8.0.0.0-28-java-8 1.0.0
export PENTAHO_PORT=8085

docker run -d \
--name ${image_name} \
-p ${PENTAHO_PORT}:8080 \
-e "ZKHELPER_URL=${ZOOKEEPER_HELPER_URL}/zookeeper/helper/znode/children/first/as-environment?path=/apps/${image_name}&globalpath=/global"  \
-e "ZKHELPER_USER=${ZOOKEEPER_HELPER_USER}" \
-e "ZKHELPER_PASSWORD=${ZOOKEEPER_HELPER_PASS}" \
-e TZ=America/Lima \
-i -t ${image_name}
```
