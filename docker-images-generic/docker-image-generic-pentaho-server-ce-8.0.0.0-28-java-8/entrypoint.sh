#!/bin/bash

mkdir -p /log
mkdir -p /env
source_code_path="/app"
log_file="/log/app.log"
jdbc_file_location=/usr/local/pentaho-server/pentaho-solutions/system/simple-jndi/jdbc.properties
kettle_properties_folder=/root/.kettle
kettle_properties_location=$kettle_properties_folder/kettle.properties


function echo_log {
  DATE='date +%Y/%m/%d:%H:%M:%S'
  echo `$DATE`" $1"
  echo `$DATE`" $1" >> $log_file
}

function export_java_env {
  java_environments=""
  echo_log "## Exporting Java Environment Variables ##"

  while IFS='=' read -r name value ; do
    if [[ $name == *'JAVA_'* ]] && [[ $name != "JAVA_HOME" ]]; then
      java_environments="$java_environments  ${!name} ";
    fi
  done < <(env)
  export CATALINA_OPTS="$java_environments"

}

function create_jdbc_properties {

  echo_log "## Creating simple-jndi/jdbc.properties ##"
  # backup previous file
  mv $jdbc_file_location $jdbc_file_location".latest"
  # restore default fie
  cp $jdbc_file_location".ini" $jdbc_file_location

  # separator
  echo "" >> $jdbc_file_location
  echo "" >> $jdbc_file_location

  while IFS='=' read -r name value ; do
    if [[ $name == *'JNDI_'* ]] ; then
      echo_log "new entry: $name"
      fixed_value=${value//[\"]/}
      echo $fixed_value >> $jdbc_file_location
    fi
  done < /env/env_variables

  ls -la /usr/local/pentaho-server/pentaho-solutions/system/simple-jndi/

}

function create_kettle_properties {

  echo_log "## Creating kettle.properties ##"
  mkdir -p $kettle_properties_folder

  if [ -e $kettle_properties_location ]; then
    # backup previous file
    mv $kettle_properties_location $kettle_properties_location".latest"
  fi

  touch $kettle_properties_location
  echo "" >> $kettle_properties_location
  echo "" >> $kettle_properties_location

  while IFS='=' read -r name value ; do
    if [[ $name == *'KETTLE_'* ]] ; then
      fixed_value=${value//[\"]/}
      echo $fixed_value >> $kettle_properties_location
    fi
  done < /env/env_variables

}


function configure_server_properties {
  echo_log "## Configure server.properties ##"
  if test -n "$FULLY_QUALIFIED_SERVER_URL"; then
    echo_log "fully-qualified-server-url=$FULLY_QUALIFIED_SERVER_URL"
    sed -i "3s/.*/fully-qualified-server-url=$FULLY_QUALIFIED_SERVER_URL/" /usr/local/pentaho-server/pentaho-solutions/system/server.properties
  fi

  echo_log "## Configure security.properties ##"
  if test -n "$ALLOW_URL_AUTHENTICATION"; then
    echo_log "ALLOW_URL_AUTHENTICATION=$ALLOW_URL_AUTHENTICATION"
    sed -i "2s/.*/requestParameterAuthenticationEnabled=$ALLOW_URL_AUTHENTICATION/" /usr/local/pentaho-server/pentaho-solutions/system/security.properties
  fi
}

function download_env_variables {

  echo_log ""
  echo_log "##### Starting to download environment variables #####"

  http_response=$(curl -s -o env_variables -w "%{http_code}" --user ${ZKHELPER_USER}:${ZKHELPER_PASSWORD} ${ZKHELPER_URL})

  echo_log "## status:  $http_response ##"
  if [ $http_response == "200" ]; then
    cp env_variables /env/
    echo_log "## Exporting Environment Variables ##"
    sort -o /env/env_variables /env/env_variables
    source /env/env_variables
    export_java_env
  else

    echo_log "## New Environment Variables could not be obtained ##"

    if [ -e /env/env_variables ]; then
      echo_log "## Exporting old env variables ##"
      source /env/env_variables
      export_java_env
    else
      echo_log "## Environment file not found in : /env/env_variables ##"
      exit 1
    fi
  fi

}

function start_app {
  echo_log ""
  echo_log "##### Starting pentaho server #####"
  echo_log ""
  bash /usr/local/pentaho-server/start-pentaho.sh |& tee -a $log_file

}

########################
# Scripts starts here
########################
download_env_variables
# create_jdbc_properties
create_kettle_properties
configure_server_properties
start_app
