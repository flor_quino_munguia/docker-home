# docker-image-generic-mule-3.9.0

This image could be used for any java 8 mule esb application whose artifact is a zip file.

# Build Steps

- copy these files to your app java source code:
    - .dockerignore
    - Dockerfile
    - entrypoint.sh

- open a shell pointing to the source code folder and build image with the following sentence:

```
export image_name=payment-engine
export nexus_user=duke
export nexus_password=java

docker build --rm -t ${image_name}:1.0.0 \
--build-arg var_nexus_username=${nexus_user}  \
--build-arg var_nexus_password=${nexus_password}  \
--build-arg var_nexus_url=https://nexus-repository.utec.edu.pe/repository/maven-public/  \
--build-arg build_command="mvn clean package -Dskiptests=true" .
```

# Tag image

- tag the image with with the following sentences:

```
export image_name=payment-engine

docker tag  ${image_name}:1.0.0 ${image_name}:1.0.0
docker tag  ${image_name}:1.0.0 ${image_name}:latest
```

# Push image to private registry

```
export image_name=payment-engine
bash /../../push-images.sh ${image_name} 1.0.0 hubdocker.utec.edu.pe
```

# Run

```
export image_name=payment-engine

docker run -d \
--name ${image_name} \
-e "ZKHELPER_URL=${ZOOKEEPER_HELPER_URL}/zookeeper/helper/znode/children/first/as-environment?path=/apps/${image_name}&globalpath=/global"  \
-e "ZKHELPER_USER=${ZOOKEEPER_HELPER_USER}" \
-e "ZKHELPER_PASSWORD=${ZOOKEEPER_HELPER_PASS}" \
-e TZ=America/Lima \
-i -t hubdocker.utec.edu.pe/${image_name}
```
