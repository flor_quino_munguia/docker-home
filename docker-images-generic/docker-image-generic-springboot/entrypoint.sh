#!/bin/bash

mkdir -p /log
mkdir -p /env
source_code_path="/app"
log_file="/log/container.log"
jar_name=${JAR_NAME}


function echo_log {
  DATE='date +%Y/%m/%d:%H:%M:%S'
  echo `$DATE`" $1"
  echo `$DATE`" $1" >> $log_file
}

function export_java_env {
  java_environments=""
  echo_log "## Exporting Java Environment Variables ##"

  while IFS='=' read -r name value ; do
    if [[ $name == *'JAVA_'* ]] && [[ $name != "JAVA_HOME" ]]; then
     java_environments="$java_environments  ${!name} ";
    fi
  done < <(env)
  export SPRINGBOOT_OPTS="$java_environments"

}

function download_env_variables {

 echo_log ""
 echo_log "##### Starting to download environment variables #####"

 http_response=$(curl -s -o env_variables -w "%{http_code}" --user ${ZKHELPER_USER}:${ZKHELPER_PASSWORD} ${ZKHELPER_URL})

 echo_log "## status:  $http_response ##"
 if [ $http_response == "200" ]; then
    cp env_variables /env/
    echo_log "## Exporting Environment Variables ##"
    source /env/env_variables
    export_java_env
 else

    echo_log "## New Environment Variables could not be obtained ##"

    if [ -e /env/env_variables ]; then
       echo_log "## Exporting old env variables ##"
       source /env/env_variables
       export_java_env
    else
       echo_log "## Environment file not found in : /env/env_variables ##"
       exit 1
    fi
 fi

}

function start_app {
  echo_log ""
  echo_log "##### Starting app ##### "
  echo_log "## source_code_path: $source_code_path ##"

  if [ -n "$jar_name" ]; then
	  jar_file_name=$jar_name
          echo_log "## jar argument detected : $jar_name ##"
  else

    if [ "$(ls -A $source_code_path)" ]; then
      cd  $source_code_path/target
      jar_file_number=$(find -name "*.jar" | wc -l)
      jar_file_name=
      if [ "$jar_file_number" -gt "1" ] ; then
        echo_log "## Several jar files were detected. We need just one!! ##"
        jar_files=$(find -name "*.jar")
        echo_log "$jar_files"
        echo_log "## You should enter a unique jar name. Example -e \"JAR_NAME=lassie.jar\" ##"
	exit 1
	else
        # get unique jar name
        jar_file_name=$(find -name "*.jar")
       fi
  fi
  fi
    cd /app/target
    echo_log "## jar to deploy: $jar_file_name ##"
    echo_log ""
    echo_log "##### Starting app #####"
    echo_log ""
    java $SPRINGBOOT_OPTS -jar $jar_file_name |& tee -a $log_file

}

 ########################
 # Scripts starts here
 ########################
download_env_variables
start_app
