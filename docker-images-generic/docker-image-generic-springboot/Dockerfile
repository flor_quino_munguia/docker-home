FROM debian-java8-maven:latest AS MAVEN_TOOL

ARG var_nexus_username
ARG var_nexus_password
ARG var_nexus_url
ARG build_command

ENV NEXUS_USERNAME=$var_nexus_username
ENV NEXUS_PASSWORD=$var_nexus_password
ENV NEXUS_URL=$var_nexus_url
ENV JAVA_TOOL_OPTIONS="$JAVA_TOOL_OPTIONS -Dfile.encoding=UTF8"

COPY . /tmp/
WORKDIR /tmp/
ENV  MAVEN_OPTS="-Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2"
RUN $build_command


FROM alpine:3.7 AS CLEAN_TOOL
ARG artifact_extension=jar
ENV ARTIFACT_EXT=$artifact_extension
COPY --from=MAVEN_TOOL /tmp/ /tmp/
RUN  mkdir app \
     && for i in `find /tmp/ -name "*.${ARTIFACT_EXT}" -print`; do cp -r $i /app ;done \
     && rm -R tmp

FROM debian-java8:latest
ADD entrypoint.sh entrypoint.sh
RUN chmod 744 entrypoint.sh
RUN apt-get update\
    && apt-get install -y curl\
    && rm -rf /var/lib/apt/lists/*

COPY --from=CLEAN_TOOL  /app/  /app/target

# override default entrypoint
ENTRYPOINT ["/entrypoint.sh"]
CMD []
