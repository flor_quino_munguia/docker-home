#!/bin/bash

mkdir -p /log
mkdir -p /env
source_code_path="/app"
log_file="/log/container.log"
zip_name=${ZIP_NAME}


function echo_log {
  DATE='date +%Y/%m/%d:%H:%M:%S'
  echo `$DATE`" $1"
  echo `$DATE`" $1" >> $log_file
}

function export_java_env {
  java_environments=""
  echo_log "## Exporting Java Environment Variables ##"

  while IFS='=' read -r name value ; do
    if [[ $name == *'JAVA_'* ]] && [[ $name != "JAVA_HOME" ]]; then
      java_environments="$java_environments  ${!name} ";
    fi
  done < <(env)
  export MULE_STANDALONE_OPTS="$java_environments"

}

function update_mule_config {
  initMemory="wrapper.java.initmemory"
  maxMemory="wrapper.java.maxmemory"
  WrapperConf="/opt/mule/conf/wrapper.conf"

  if [ "$MULE_STANDALONE_OPTS" != "" ];then
    sed -i "s/${initMemory}=1024/${initMemory}=0/g" ${WrapperConf}
    sed -i "s/${maxMemory}=1024/${maxMemory}=0/g" ${WrapperConf}
  fi
}

function download_env_variables {

  echo_log ""
  echo_log "##### Starting to download environment variables #####"

  http_response=$(curl -s -o env_variables -w "%{http_code}" --user ${ZKHELPER_USER}:${ZKHELPER_PASSWORD} ${ZKHELPER_URL})

  echo_log "## status:  $http_response ##"
  if [ $http_response == "200" ]; then
    cp env_variables /env/
    echo_log "## Exporting Environment Variables ##"
    # add ZKHELPER_URL
    echo 'export ZKHELPER_URL=$ZKHELPER_URL'>> /env/env_variables
    source /env/env_variables
    export_java_env
  else

    echo_log "## New Environment Variables could not be obtained ##"

    if [ -e /env/env_variables ]; then
      echo_log "## Exporting old env variables ##"
      source /env/env_variables
      export_java_env
    else
      echo_log "## Environment file not found in : /env/env_variables ##"
      exit 1
    fi
  fi

}

function start_app {
  echo_log ""
  echo_log "##### Starting app ##### "
  echo_log "## source_code_path: $source_code_path ##"

  if [ -n "$zip_name" ]; then
    zip_file_name=$zip_name
    echo_log "## zip argument detected : $zip_name ##"
  else

    if [ "$(ls -A $source_code_path)" ]; then
      cd  $source_code_path/target
      zip_file_number=$(find -name "*.zip" | wc -l)
      zip_file_name=
      if [ "$zip_file_number" -gt "1" ] ; then
        echo_log "## Several zip files were detected. We need just one!! ##"
        zip_files=$(find -name "*.zip")
        echo_log "$zip_files"
        echo_log "## You should enter a unique zip name. Example -e \"ZIP_NAME=lassie.zip\" ##"
        exit 1
      else
        # get unique war name
        zip_file_name=$(find -name "*.zip")
      fi
    fi
  fi
  cd /app/target
  echo_log "## zip to deploy: $zip_file_name ##"

  if [ -d "/opt/mule/apps/ROOT" ]
  then
    echo_log "## App is already deployed. ##"
  else
    echo_log "## Moving zip to apps ##"
    echo_log "## cp $zip_file_name /opt/mule/apps/ROOT.zip ##"
    rm -rf /opt/mule/apps/ROOT.zip
    cp $zip_file_name /opt/mule/apps/ROOT.zip
  fi



  echo_log ""
  echo_log "##### Starting MULE #####"
  echo_log ""
  echo_log "MULE_STANDALONE_OPTS:$MULE_STANDALONE_OPTS"
  bash /opt/mule/bin/mule console $MULE_STANDALONE_OPTS |& tee -a $log_file

}

 ########################
 # Scripts starts here
 ########################
 download_env_variables
 update_mule_config
 start_app
