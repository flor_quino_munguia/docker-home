REAL_EXECUTABLE_LOCATION=$(realpath "$0")
DOCKER_HOME_PATH=$(dirname "$REAL_EXECUTABLE_LOCATION")
DOCKER_HELPER_HOME_CONFIG=$HOME/.docker-helper
echo ""
echo ""
echo "Executable location: $REAL_EXECUTABLE_LOCATION"
echo "Docker Home Path:$DOCKER_HOME_PATH"
echo ""
echo ""

function validate_required_values {

  echo "Validating required configurations"

  if test -f "$DOCKER_HELPER_HOME_CONFIG/environment"; then
    echo "$DOCKER_HELPER_HOME_CONFIG/environment exist"
  else
    if test -f "$DOCKER_HOME_PATH/docker-config/ports.properties"; then
      echo "$DOCKER_HOME_PATH/docker-config/ports.properties exist"
    else
      echo ""
      echo "These files are required: $DOCKER_HELPER_HOME_CONFIG/environment, $DOCKER_HOME_PATH/docker-config/ports.properties "
      echo "Execute docky option 0"
      echo "To reload or fix the configuration"
      exit 1
    fi
  fi

}

function export_configurations {

  # exporting ports as env parameters
  if test -f "$DOCKER_HOME_PATH/docker-config/ports.properties"; then
    echo "$DOCKER_HOME_PATH/docker-config/ports.properties exist"
    while read line_info; do

      if [ -z "$line_info" ]
      then
        continue
      fi

      if [[ ! $line_info =~ [^[:space:]] ]] ; then
        continue
      fi

      count=0
      app_name="";
      app_port="";
      for line_info_tmp in $(echo $line_info | sed "s/=/ /g")
      do
        if [ $count -eq 0 ]; then
          app_name=$line_info_tmp
        fi

        if [ $count -eq 1 ]; then
          app_port=$line_info_tmp
        fi
        (( count++ ))
      done

      export_string="export $app_name=$app_port"
      eval $export_string

    done <$DOCKER_HOME_PATH/docker-config/ports.properties

    # exporting env parameters
    source $DOCKER_HELPER_HOME_CONFIG/environment
  else
    echo ""
    echo "$DOCKER_HOME_PATH/docker-config/ports.properties does not exist"
    echo "Update docker-home and try again!"
    exit 1
  fi

}

## choices ##

function configure_docky {
  if [ -d "$DOCKER_HELPER_HOME_CONFIG" ]
  then
    echo "Assistant is already configured"
    echo "If you need to update configuration with latest version, delete this file: $DOCKER_HELPER_HOME_CONFIG"
    echo "and execute again docky opcion 0"
  else
    echo "Hi. This is your first with this assistant. I will configure the requirements..."
    sleep 5
    mkdir -p $DOCKER_HELPER_HOME_CONFIG
    cp $DOCKER_HOME_PATH/docker-config/environment $DOCKER_HELPER_HOME_CONFIG
    ls -la $DOCKER_HELPER_HOME_CONFIG
    echo ""
    echo "Done!!. Please check this folder: $DOCKER_HELPER_HOME_CONFIG and configure your variables before use another commands!"
    echo ""
    echo ""
    exit 0
  fi
}

function start_specific_apps_with_compose {

  echo ""
  echo ""

  cd $DOCKER_HOME_PATH/docker-apps-compose
  if [ -z "$1" ]
  then
    echo "Enter the name of applications separated by spaces"
    read APPLICATIONS
  else
    APPLICATIONS=$1
  fi

  validate_required_values
  export_configurations

  # start apps
  for app in ${APPLICATIONS//;/ }
  do
     echo "analizing : $app"
    container_exist=$(docker ps -q --filter "name=$app")
    if [ -z "$container_exist" ]
    then
      echo "container is not running"
      container_exited=$(docker ps -a -q --filter "name=$app")
      if [ -z "$container_exited" ]
      then
        echo "container is not running an does not exist"
      else
        image_sha=$(docker inspect --format '{{ index .Image}}' $app)
        (
        echo "deleting container..."
        docker rm -f $app
        )
        (
        echo "deleting image..."
        docker rmi $image_sha -f
        )
      fi
    else
      image_sha=$(docker inspect --format '{{ index .Image}}' $app)
      echo "container is running"
      (
      echo "stoping container..."
      docker stop $app
      )
      (
      echo "deleting container..."
      docker rm -f $app
      )
      (
      echo "deleting image..."
      docker rmi $image_sha -f
      )
    fi
    echo "starting app: $app"
    docker-compose up -d "$app"
  done

}

function build_and_deploy_source_code {

  for ARGUMENT in "$@"
  do

      KEY=$(echo $ARGUMENT | cut -f1 -d:)
      VALUE=$(echo $ARGUMENT | cut -f2 -d:)

      case "$KEY" in
              docker_img_generic)      docker_img_generic=${VALUE} ;;
              source_code_path)        source_code_path=${VALUE} ;;
              image_name)              image_name=${VALUE} ;;
              container_port)          container_port=${VALUE} ;;
              build_command)           build_command=${VALUE} ;;
              additional_arguments)    additional_arguments=${VALUE} ;;
              war_name)                war_name=${VALUE} ;;
              *)
      esac

  done

  echo "Arguments"
  echo ""

  echo "docker_img_generic:$docker_img_generic"
  echo "source_code_path:$source_code_path"
  echo "image_name:$image_name"
  echo "container_name:$container_name"
  echo "container_port:$container_port"
  echo "war_name:$war_name"
  echo "build_command:$build_command"
  echo "additional_arguments:$additional_arguments"

  docker_img_generic_path="$DOCKER_HOME_PATH/docker-images-generic/$docker_img_generic"

  echo ""
  echo ""

  validate_required_values
  export_configurations

  echo ""
  echo "Build and deploy source code is starting..."
  echo ""

  # build
  WORKSPACE_TMP_PATH="/tmp/"$(uuidgen)
  mkdir -p $WORKSPACE_TMP_PATH
  echo "Workspace:$WORKSPACE_TMP_PATH"

  cp -R $source_code_path/* $WORKSPACE_TMP_PATH
  cp -R $docker_img_generic_path/* $WORKSPACE_TMP_PATH
  ls -la $WORKSPACE_TMP_PATH

  cd $WORKSPACE_TMP_PATH
  docker build --no-cache --rm -t "${image_name}:SNAPSHOT" \
  --build-arg var_nexus_username=${nexus_user}  \
  --build-arg var_nexus_password=${nexus_password}  \
  --build-arg var_nexus_url=https://nexus-repository.utec.edu.pe/repository/maven-public/  \
  --build-arg build_command="$build_command" \
  --build-arg $additional_arguments .

  docker rm $container_name -f

  #Run
  docker run -d \
  --name ${container_name} \
  -p $container_port:8080 \
  -e "ZKHELPER_URL=${ZOOKEEPER_HELPER_URL}/zookeeper/helper/znode/children/first/as-environment?path=/apps/${container_name}&globalpath=/global"  \
  -e "ZKHELPER_USER=${ZOOKEEPER_HELPER_USER}" \
  -e "ZKHELPER_PASSWORD=${ZOOKEEPER_HELPER_PASS}" \
  -e "WAR_NAME=$war_name" \
  -e TZ=America/Lima \
  -i -t "${image_name}:SNAPSHOT"

}

function view_variables {
 cat $DOCKER_HELPER_HOME_CONFIG/environment
}


function start_interactive_shell {

  choice="$1"
  applications="$2"

  if [ ! -z "$choice" ]
  then
    echo "interactive shell is not required."
  else
    echo ""
    echo "Hi"
    echo "Which choice would you like?"
    echo ""
    echo "0 : configure docky"
    echo "1 : start specific applications using docker-compose"
    echo "2 : build and deploy app"
    echo "3 : view variables"
    echo ""
    echo ""
    read choice
  fi

  case "$choice" in

    0)
      configure_docky
      ;;
    1)
      start_specific_apps_with_compose "${@:2}"
      ;;
    2)
      build_and_deploy_source_code "${@:2}"
      ;;
    3)
      view_variables
      ;;
    *)
      echo "$choice is not a valid option"
      ;;
  esac

}

########################
# Execution starts here
########################
start_interactive_shell "$@"
