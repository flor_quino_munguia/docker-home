# add repositories as submodules

git submodule add git@bitbucket.org:utecsup/debian-java8-tomcat.git docker-images-base/debian-java8-tomcat
git submodule add git@bitbucket.org:utecsup/debian-java8-pentaho-server-ce-8.0.0.0-28.git docker-images-base/debian-java8-pentaho-server-ce-8.0.0.0-28

# update them

git submodule update --remote --merge
git submodule update --init
